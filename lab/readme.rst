
Laboratorium #3
===============

1. Posortować oceny wraz z ich nazwami i linkami do zadań w tylu listach ile jest kategorii ocen.
   Wyświetlić posortowane oceny od najlepszej do najgorszej w każdej kategorii osobno.
   Program należy umieścić w pliku ``my_grades.py``.

2. Mashup. Dla adresu podanego przez użytkownika znaleźć najbliższe geoskrytki oraz, korzystając z serwisu google'a
   z odwróconym geokodowaniem (https://developers.google.com/maps/documentation/geocoding/#ReverseGeocoding),
   przydzielić im rzeczywiste adresy. W pierwszej kolejności wyświetlić dostępne skrytki.
   Program należy umieścić w pliku ``nearest_caches.py``
